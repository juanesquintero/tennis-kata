import re
import os
import sys
import logging

error_logger = logging.getLogger('error_logger')

def exception(op):
    if isinstance(op, Exception): 
        error_logger.error('EXCEPTION: '+str(op),exc_info=True)
        return {'error': 'Ha ocurrido un error en la ejecución del servidor, si es necesario contacte al Administrador del sistema para verificar el error.'}, 500 
    else:
        return False
        
def clean_exception(exp):
    ip_port = '[0-9]+(?:\.[0-9]+){3}(:[0-9]+)?'
    error = re.sub(ip_port,'',str(exp))
    
    words = ['MYSQL', 'SQL', 'MARIADB', 'MICROSOFT','SQL SERVER','ODBC']
    for word in words:
        error = re.sub(word+'(?i)','',error)
    
    symbols = ['[',']','"']    
    for symbol in symbols:
        error = error.replace(symbol,'')

    return error