import unittest

from game import TennisGame

def play_game(p1Points, p2Points, p1Name, p2Name):
    game = TennisGame(p1Name, p2Name)
    for _ in range(p1Points):
        game.won_point(p1Name)
    for _ in range(p2Points):
        game.won_point(p2Name)
    return game

class TestTennis(unittest.TestCase):

    def test_tie(self):
        game = play_game(2, 2, 'player1', 'player2')
        self.assertEqual("Thirty-All", game.score())

    def test_deuce(self):
        game = play_game(4, 4, 'player1', 'player2')
        self.assertEqual("Deuce", game.score())

    def test_winner(self):
        game = play_game(6, 4, 'player1', 'player2')
        self.assertEqual("Win for player2", game.score())

if __name__ == "__main__":
    unittest.main()
